var Index = function () {
  var toggleColor = function () {
    $('#agence').on('click', function () {
      $('body').toggleClass('agence');
    });
  };

  return {
    init: function () {
      toggleColor();
    }
  };
}();

$(document).ready(function () {
  Index.init();
});
